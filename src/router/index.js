import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/views/login'
import Home from '@/views/Home'
import Welcome from '@/views/Welcome'
import Users from '@/views/Users'
import Rights from '@/views/power/Rights'
import Roles from '@/views/power/Roles'
import Cate from '@/views/goods/Cate'
import Params from '@/views/goods/Params'
import List from '@/views/goods/List'
import Add from '@/views/goods/Add'
import Orders from '@/views/Orders'
import Reports from '@/views/Reports'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: login },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/users', component: Users },
      { path: '/rights', component: Rights },
      { path: '/roles', component: Roles },
      { path: '/categories', component: Cate },
      { path: '/params', component: Params },
      { path: '/goods', component: List },
      { path: '/goods/add', component: Add },
      { path: '/orders', component: Orders },
      { path: '/reports', component: Reports }
    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    next()
  } else {
    const tokenStr = sessionStorage.getItem('token')
    if (tokenStr) {
      next()
    } else {
      next('/login')
    }
  }
})

export default router
